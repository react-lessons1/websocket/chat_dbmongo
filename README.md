lesson = 'https://www.youtube.com/watch?v=SDMs2Pq6w90'

### Server:

1. express
2. cors
3. nodemon
4. socket.io
5. morgan
6. body-parser
7. dotenv

### Socket server:

1. socket.io
2. nodemon

### Client:

1. socket.io-client
2. react-router-dom
3. axios
4. timeago.js - for to format the DateTime output
5. react-input-emoji

### history

1. ver.1.0 - chat with DB mongo

import { createBrowserRouter } from 'react-router-dom';

import { App } from 'src/components/App';

import { Chat } from 'src/pages/Chat';
import { Home } from 'src/pages/Home';

export const router = createBrowserRouter([
  {
    element: <App />,
    path: '/',
    errorElement: <div>Error 404</div>,
    children: [
      {
        element: <Home />,
        index: true,
      },
      {
        element: <Chat />,
        path: 'chat',
      },
    ],
  },
]);

import { useEffect, useRef, useState } from 'react';
import { userChats } from 'src/api/ChatRequest';
import { ChatBox } from 'src/components/ChatBox';
import { Conversation } from 'src/components/Conversation';
import { io } from 'socket.io-client';

import styles from 'src/styles/Chat.module.css';
// import { styles } from 'src/styles/Home.module.css';

// TODO: remove obj-user when add user from redux
const user = {
  _id: '123',
  firstname: 'Alex',
  lastname: 'Bondarenko',
  username: 'zain@gmail.com',
  coverPicture: '',
  followers: [],
  following: [],
  profilePicture: '',
  relationship: 'in relationship',
};

export const Chat = () => {
  // TODO: get user-owner from redux
  // const {user} = useSelector((state)=> {state.authReducer.authData})

  // set chats where user be
  const [chats, setChats] = useState([]);

  // current chat
  const [currentChat, setCurrentChat] = useState(null);

  // set online guest-users
  const [onlineUsers, setOnlineUsers] = useState([]);

  // set message for socket server
  const [sendMessage, setSendMessage] = useState(null);

  // receive message from socket server
  const [receiveMessage, setReceiveMessage] = useState(null);

  // set socket
  // использования useRef для хранения объекта сокета
  // Сохранение ссылки на изменяемый объект между рендерами
  // Изменение значения без вызова перерендера
  // Ссылка на актуальное значение: CURRENT свойство объекта useRef всегда содержит актуальное значение,
  // даже если это значение было изменено
  const socket = useRef();

  // create socket
  // TODO: set change socket server url
  useEffect(() => {
    socket.current = io('http://localhost:8800');
    socket.current.emit('add-new-user', user._id);
    socket.current.on('get-users', (users) => {
      setOnlineUsers(users);
    });
  }, [user]);

  // send message to socket server
  useEffect(() => {
    if (sendMessage !== null) {
      socket.current.emit('send-message', sendMessage);
    }
  }, [sendMessage]);

  // receive message from socket server
  useEffect(() => {
    socket.current.on('receive-message', (data) => {
      setReceiveMessage(data);
    });
  }, []);

  // set current chat
  const handleCurrentChat = (chat) => {
    setCurrentChat(chat);
  };

  // get chats
  useEffect(() => {
    const getChats = async () => {
      try {
        // request chats from server
        const { data } = await userChats(user._id);

        // set chats
        setChats(data);
      } catch (error) {
        console.log(error);
      }
    };

    getChats();
  }, []);

  // check online users
  const checkOnlineStatus = (chat) => {
    // find guest-user
    const chatMember = chat.members.find((member) => member !== user._id);

    // find is guest-user is onlineUsers
    const online = onlineUsers.find((user) => user.userId === chatMember);

    return online ? true : false;
  };

  return (
    <div className={styles.Chat}>
      {/* left panel of chat */}
      <div className={styles.LeftSideChat}>
        <div className={styles}>Search ...</div>
        <div className={styles.ChatContainer}>
          <h4>Chats</h4>
          <div className={styles.ChatList}>
            {chats.map((chat, index) => (
              <div
                className="xxx"
                key={index}
                onClick={() => handleCurrentChat(chat)}
              >
                <Conversation
                  data={chat}
                  currentUserId={user._id}
                  online={checkOnlineStatus(chat)}
                />
              </div>
            ))}
          </div>
        </div>
      </div>

      {/* chat body */}
      <div className={styles.RightSideChat}>
        <ChatBox
          chat={currentChat}
          currentUser={user._id}
          setSendMessage={setSendMessage}
          receiveMessage={receiveMessage}
        />
      </div>
    </div>
  );
};

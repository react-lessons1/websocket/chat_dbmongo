import axios from 'axios';

// TODO: added axios configuration
const API = axios.create({ baseURL: 'http://localhost:5000' });

// request chats from server
export const userChats = (id) => API.get(`/chat/${id}`);

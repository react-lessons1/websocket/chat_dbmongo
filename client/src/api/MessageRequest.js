import axios from 'axios';

// TODO: added axios configuration
const API = axios.create({ baseURL: 'http://localhost:5000' });

// request messages from server
export const getMessages = (id) => API.get(`/message/${id}`);

// send messages for server
export const addMessage = (data) => API.post('/message/', data);

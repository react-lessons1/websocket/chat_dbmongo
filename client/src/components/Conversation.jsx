import { useState, useEffect } from 'react';

import styles from 'src/styles/Chat.module.css';

// get other users
export const Conversation = ({ data, currentUserId, online }) => {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    // find other users
    const userId = data.members.find((id) => id !== currentUserId);

    // get users data
    const getUserData = async () => {
      try {
        // TODO: getUser(id): get users from server in redux, path: /user/:userId
        // const { data } = await getUser(userId);
        // TODO: delete this obj when create request in redux
        const data = {
          _id: '456',
          firstname: 'Oleg',
          lastname: 'Semenov',
          username: 'oleg@gmail.com',
          coverPicture: '',
          followers: [],
          following: [],
          profilePicture: '',
          relationship: 'in relationship',
        };
        // TODO: end obj

        setUserData(data);
      } catch (error) {
        console.log(error);
      }
    };

    getUserData();
  }, [currentUserId, data]);
  return (
    <div className={`${styles.follower} ${styles.conversation}`}>
      {online && <div className={styles.onlineDot}></div>}
      <div>userLogo</div>
      <div>
        {userData?.firstname} {userData?.lastname}
      </div>
      <div>{online ? 'online' : 'offline'}</div>
    </div>
  );
};

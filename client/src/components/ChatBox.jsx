import { useState, useEffect, useRef } from 'react';
import { format } from 'timeago.js';
import InputEmoji from 'react-input-emoji';

import { addMessage, getMessages } from 'src/api/MessageRequest';

import styles from 'src/styles/ChatBox.module.css';

export const ChatBox = ({
  chat,
  currentUser,
  setSendMessage,
  receiveMessage,
}) => {
  // set data about the guest
  const [userData, setUserData] = useState(null);

  // set ChatBox messages
  const [messages, setMessages] = useState([]);

  // set newMessage for send
  const [newMessage, setNewMessage] = useState('');

  // setting browser-scroll to useRef() to automatically scroll when message is outside
  const scroll = useRef();

  // get receiveMessage from socket server
  useEffect(() => {
    if (receiveMessage !== null && receiveMessage.chatId === chat._id) {
      setMessages([...messages, receiveMessage]);
    }
  }, [receiveMessage]);

  // getting data for the header about the guest
  useEffect(() => {
    const userId = chat?.members?.find((id) => id !== currentUser);
    const getUserData = async () => {
      try {
        // TODO: getUser(id): get users from server in redux, path: /user/:userId
        // const { data } = await getUser(userId);
        // TODO: delete this obj when create request in redux
        const data = {
          _id: '456',
          firstname: 'Oleg',
          lastname: 'Semenov',
          username: 'oleg@gmail.com',
          coverPicture: '',
          followers: [],
          following: [],
          profilePicture: '',
          relationship: 'in relationship',
        };
        // TODO: end obj

        setUserData(data);
      } catch (error) {
        console.log(error);
      }
    };

    if (chat !== null) getUserData();
  }, [chat, currentUser]);

  // getting data for messages
  useEffect(() => {
    const fetchMessages = async () => {
      try {
        const { data } = await getMessages(chat._id);

        setMessages(data);
      } catch (error) {
        console.log(error);
      }
    };
    if (chat !== null) fetchMessages();
  }, [chat]);

  // emoji
  const handleEmojiChange = (newMessage) => {
    setNewMessage(newMessage);
  };

  // send message to server
  const handleSend = async (e) => {
    e.preventDefault();
    const message = {
      chatId: chat._id,
      senderId: currentUser,
      text: newMessage,
    };

    // send message to DB
    try {
      const { data } = await addMessage(message);

      setMessages([...messages, data]);
      setNewMessage('');
    } catch (error) {
      console.log(error);
    }

    // send message to socket server
    const receiverId = chat.members.find((id) => id !== currentUser);
    setSendMessage({ ...message, receiverId });
  };

  // always scroll to last message
  useEffect(() => {
    scroll.current?.scrollIntoView({ behavior: 'smooth' });
  }, [messages]);

  return (
    <>
      <div className={styles.ChatBoxContainer}>
        {chat ? (
          <>
            <div className={styles.chatHeader}>
              <div className={styles.follower}>
                <div>
                  info: {userData?.firstname} {userData?.lastname}
                </div>
              </div>
            </div>

            {/* chatbox messages */}
            <div className={styles.chatBody}>
              {messages.map((message, index) => (
                <div key={index}>
                  <div
                    ref={scroll}
                    className={
                      message.senderId === currentUser
                        ? `${styles.message} ${styles.own}`
                        : `${styles.message}`
                    }
                  >
                    <span>{message.text}</span>
                    <span>{format(message.createdAt)}</span>
                  </div>
                </div>
              ))}
            </div>

            {/* chat-sender */}
            <div className={styles.chatSender}>
              <div className={styles.plusBtn}>+</div>
              <InputEmoji value={newMessage} onChange={handleEmojiChange} />
              <div
                className={`${styles.sendButton} ${styles.button}`}
                onClick={handleSend}
              >
                send
              </div>
            </div>
          </>
        ) : (
          <span className={styles.chatboxEmptyMessage}>
            Tap on a chat to start conversation...
          </span>
        )}
      </div>
    </>
  );
};

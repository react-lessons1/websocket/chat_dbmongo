import { NavLink } from 'react-router-dom';

import styles from 'src/styles/Menu.module.css';

export const Menu = () => {
  return (
    <nav className={styles.MenuContainer}>
      <ul className={styles.Menu}>
        <li className={styles.MenuItem}>
          <NavLink to="/">Home</NavLink>
        </li>
        <li className={styles.MenuItem}>
          <NavLink to="/chat">Chat</NavLink>
        </li>
      </ul>
    </nav>
  );
};

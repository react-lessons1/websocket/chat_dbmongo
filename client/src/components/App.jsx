import { Outlet } from 'react-router-dom';

import styles from 'src/styles/App.module.css';
import { Menu } from './Menu';

export const App = () => {
  return (
    <div className={styles.wrapper}>
      <header className={styles.head}>
        <Menu />
      </header>
      <main className={styles.main}>
        <Outlet />
      </main>
    </div>
  );
};

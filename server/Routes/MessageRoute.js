import { Router } from 'express';
import { addMessage, getMessage } from '../Controllers/MessageController.js';

export const MessageRoute = new Router();

MessageRoute.post('/', addMessage);
MessageRoute.get('/:chatId', getMessage);

import { Router } from 'express';
import {
  createChat,
  findChat,
  userChats,
} from '../Controllers/ChatController.js';

export const ChatRoute = new Router();

ChatRoute.post('/', createChat);
ChatRoute.get('/:userId', userChats);
ChatRoute.get('/find/:firstId/:secondId', findChat);
